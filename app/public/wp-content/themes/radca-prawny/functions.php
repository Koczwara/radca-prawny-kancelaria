<?php

function radca_files() {
  wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700&family=Roboto:wght@100;300;400;500&display=swap','//fonts.googleapis.com/css2?family=Open+Sans:wght@300;400&display=swap' );
  wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
  
  if (strstr($_SERVER['SERVER_NAME'], 'radca-prawny.local')) {
    wp_enqueue_script('main-radca-js', 'http://localhost:3000/bundled.js', NULL, '1.0', true);
  } else {
    wp_enqueue_script('our-vendors-js', get_theme_file_uri('/bundled-assets/vendors~scripts.a6c6785452837b740890.js'), NULL, '1.0', true);
    wp_enqueue_script('main-radca-js', get_theme_file_uri('/bundled-assets/scripts.0b2086c557e03ec33efe.js'), NULL, '1.0', true);
    wp_enqueue_style('our-main-styles', get_theme_file_uri('/bundled-assets/styles.0b2086c557e03ec33efe.css'));
    wp_enqueue_style('our-vendors-styles', get_theme_file_uri('/bundled-assets/styles.0b2086c557e03ec33efe.css'));
  }
  wp_localize_script('main-radca-js', 'radcaData', array(
    'root_url' => get_site_url()
  ));
  
}

add_action('wp_enqueue_scripts', 'radca_files');

function radca_features() {
  register_nav_menu('headerMenuLocation', 'Header Menu Location');
  add_theme_support('title-tag');
}

add_action('after_setup_theme', 'radca_features');

