<?php
  $services_img = get_field('services_img');
  $services_title = get_field('services_title');
  $services_description = get_field('services_description');
  $services_item_one = get_field('services_item_one');
  $services_item_two = get_field('services_item_two');
  $services_item_three = get_field('services_item_three');
  $services_item_four = get_field('services_item_four');
  $services_item_five = get_field('services_item_five');
  $services_item_six = get_field('services_item_six');
  $services_item_seven = get_field('services_item_seven');
  $services_item_eight = get_field('services_item_eight');
  $service_box_one_title = get_field('service_box_one_title');
  $service_box_two_title = get_field('service_box_two_title');
  $service_box_three_title = get_field('service_box_three_title');
  $service_box_four_title = get_field('service_box_four_title');
  $service_box_five_title = get_field('service_box_five_title');
  $service_box_six_title = get_field('service_box_six_title');
  $service_box_button = get_field('service_box_button');
  $service_box_subtitle = get_field('service_box_subtitle');
  $service_box_one_point_one = get_field('service_box_one_point_one');
  $service_box_one_point_two = get_field('service_box_one_point_two');
  $service_box_one_point_three = get_field('service_box_one_point_three');
  $service_box_one_point_four = get_field('service_box_one_point_four');
  $service_box_two_point_one = get_field('service_box_two_point_one');
  $service_box_two_point_two = get_field('service_box_two_point_two');
  $service_box_two_point_three = get_field('service_box_two_point_three');
  $service_box_two_point_four = get_field('service_box_two_point_four');
  $service_box_three_point_one = get_field('service_box_three_point_one');
  $service_box_three_point_two = get_field('service_box_three_point_two');
  $service_box_three_point_three = get_field('service_box_three_point_three');
  $service_box_three_point_four = get_field('service_box_three_point_four');
  $service_box_four_point_one = get_field('service_box_four_point_one');
  $service_box_five_point_one = get_field('service_box_five_point_one');
  $service_box_six_point_one = get_field('service_box_six_point_one');
?>

<div class="container services--container" id="services">
    <div class="row">
            <div class="services__headline services-headline">
                <h1 class="services-title" data-aos="zoom-in"  data-aos-duration="1000" data-aos-once="true">
                    <?= $services_title ?>
                </h1>         
        </div>
        <div class="col-lg-6 services-main--description--container">
            <p class="services-main--description">
                <?= $services_description ?>
            </p>
        </div>
    </div>

    <div class="training__items">
        <div class="training__list">
            <span data-aos="fade-right" data-aos-duration="500" data-aos-once="true">
                <?= $services_item_one ?>
            </span>
            <span data-aos="fade-right" data-aos-duration="700" data-aos-once="true">
                <?= $services_item_two ?>
            </span>
            <span data-aos="fade-right" data-aos-duration="900" data-aos-once="true">
                <?= $services_item_three ?>
            </span>
            <span data-aos="fade-right" data-aos-duration="1100" data-aos-once="true">
                <?= $services_item_four ?>
            </span>
            <span data-aos="fade-right" data-aos-duration="1300" data-aos-once="true">
                <?= $services_item_five ?>
            </span>
            <span data-aos="fade-right" data-aos-duration="1500" data-aos-once="true">
                <?= $services_item_six ?>
            </span>
            <span data-aos="fade-right" data-aos-duration="1700" data-aos-once="true">
                <?= $services_item_seven ?>
            </span>
            <span data-aos="fade-right" data-aos-duration="1900" data-aos-once="true">
                <?= $services_item_eight ?>
            </span>
        </div>
    </div>
</div>
<section class="services-banner" style="background-image: url(<?= $services_img ?>);">  
    <div class="services-banner__overlay">
        <div class="container">
            <div class="row">
                <div class="col">
                <div id="top"></div>
                    <section class="services">
                        <div class="services-row">
                            <div class="row law-container">
                                <a href="#" class="close"></a>
                                <?php if (!empty($service_box_one_title)) { ?>
                                <div class="col-lg-4 law-box">
                                    <div class="li">
                                        <img src="<?php echo get_theme_file_uri('/images/family.svg')?>" class="law-img" alt="prawo cywilne" loading="lazy">
                                        <h1 class="services-box--title">
                                            <?= $service_box_one_title ?>
                                        </h1>
                                        <a href="#item01">
                                            <button class="services-button">
                                                <?= $service_box_button ?>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php if (!empty($service_box_two_title)) { ?>
                                <div class="col-lg-4 law-box">
                                    <div class="li">
                                        <img src="<?php echo get_theme_file_uri('/images/hammer.svg')?>" class="law-img" alt="prawo karne" loading="lazy">
                                        <h1 class="services-box--title">
                                            <?= $service_box_two_title ?>
                                        </h1>
                                        <a href="#item02">
                                            <button class="services-button">
                                                <?= $service_box_button ?>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php if (!empty($service_box_three_title)) { ?>
                                <div class="col-lg-4 law-box">
                                    <div class="li">
                                        <img src="<?php echo get_theme_file_uri('/images/heands.svg')?>" class="law-img" alt="prawo pracy i ubezpieczeń społecznych" loading="lazy">
                                        <h1 class="services-box--title">
                                            <?= $service_box_three_title ?>
                                        </h1>
                                        <a href="#item03">
                                            <button class="services-button">
                                                <?= $service_box_button ?>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php if (!empty($service_box_four_title)) { ?>
                                <div class="col-lg-4 law-box">
                                    <div class="li">
                                        <img src="<?php echo get_theme_file_uri('/images/weight.svg')?>" class="law-img" alt="prawo administracyjne" loading="lazy">
                                        <h1 class="services-box--title">
                                            <?= $service_box_four_title ?> 
                                        </h1>
                                        <a class="image" href="#item04">
                                            <button class="services-button">
                                                <?= $service_box_button ?>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php if (!empty($service_box_five_title)) { ?>
                                <div class="col-lg-4 law-box">
                                    <div class="li">
                                        <img src="<?php echo get_theme_file_uri('/images/money.svg')?>" class="law-img" alt="odszkodowania" loading="lazy">
                                        <h1 class="services-box--title">
                                            <?= $service_box_five_title ?>
                                        </h1>
                                        <a class="image" href="#item05">
                                            <button class="services-button">
                                                <?= $service_box_button ?>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php if (!empty($service_box_six_title)) { ?>
                                <div class="col-lg-4 law-box">
                                    <div class="li">
                                        <img src="<?php echo get_theme_file_uri('/images/agreement.svg')?>" class="law-img" alt="prawo gospodarcze" loading="lazy">
                                        <h1 class="services-box--title">
                                            <?= $service_box_six_title ?>
                                        </h1>
                                        <a class="image" href="#item06">
                                            <button class="services-button">
                                                <?= $service_box_button ?>
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <?php } ?>
                                
                            </div>
                        </div> 


                        <div id="item01" class="port">                
                            <div class="port-container">
                                <h1>
                                    <?= $service_box_one_title ?>
                                </h1>
                                <p>
                                    <?= $service_box_subtitle ?>
                                </p>
                                <div class="training__items">
                                    <div class="training__list">
                                        <span>
                                            <?= $service_box_one_point_one ?>
                                        </span>
                                        <span>
                                            <?= $service_box_one_point_two ?>
                                        </span>
                                        <span>
                                            <?= $service_box_one_point_three ?>
                                        </span>
                                        <span>
                                            <?= $service_box_one_point_four ?>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div> 
                        <div id="item02" class="port">                
                            <div class="port-container">
                                <h1>
                                    <?= $service_box_two_title ?>
                                </h1>
                                <p>
                                    <?= $service_box_subtitle ?>
                                </p>
                                <div class="training__items">
                                    <div class="training__list">
                                        <span>
                                            <?= $service_box_two_point_one ?>
                                        </span>
                                        <span>
                                            <?= $service_box_two_point_two ?>
                                        </span>
                                        <span>
                                            <?= $service_box_two_point_three ?>
                                        </span>
                                        <span>
                                            <?= $service_box_two_point_four ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div id="item03" class="port">                
                            <div class="port-container">
                                <h1>
                                    <?= $service_box_three_title ?>
                                </h1>
                                <p>
                                    <?= $service_box_subtitle ?>
                                </p>
                                <div class="training__items">
                                    <div class="training__list">
                                        <span>
                                            <?= $service_box_three_point_one ?>
                                        </span>
                                        <span>
                                            <?= $service_box_three_point_two ?>
                                        </span>
                                        <span>
                                            <?= $service_box_three_point_three ?>
                                        </span>
                                        <span>
                                            <?= $service_box_three_point_four ?>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div> 
                        <div id="item04" class="port">                
                            <div class="port-container">
                                <h1>
                                    <?= $service_box_four_title ?>
                                </h1>
                                <p>
                                    <?= $service_box_subtitle ?>
                                </p>
                                <div class="training__items">
                                    <div class="training__list">
                                        <span>
                                            <?= $service_box_four_point_one ?>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div> 
                        <div id="item05" class="port">                
                            <div class="port-container">
                                <h1>
                                    <?= $service_box_five_title ?>
                                </h1>
                                <p>
                                    <?= $service_box_subtitle ?>
                                </p>
                                <div class="training__items">
                                    <div class="training__list">
                                        <span>
                                            <?= $service_box_five_point_one ?>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div> 
                        <div id="item06" class="port">                
                            <div class="port-container">
                                <h1>
                                 <?= $service_box_six_title ?>
                                </h1>
                                <p>
                                    <?= $service_box_subtitle ?>
                                </p>
                                <div class="training__items">
                                    <div class="training__list">
                                        <span>
                                            <?= $service_box_five_point_one ?>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div> 
                    </section>
                </div>
            </div>
        </div>
        </div>
    </div>
</section>