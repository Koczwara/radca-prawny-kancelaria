<?php
  $main_img = get_field('main_img');
  $main_title = get_field('main_title');
  $main_description = get_field('main_description');
  $main_button = get_field('main_button');
?>


<section id="home" class="page-banner" style="background-image: url( <?= $main_img ?>);">
   <div class="page-banner__overlay">
      <div class="">
         <img src="<?php echo get_theme_file_uri('/images/logo.png')?>" class="logo" alt="logo" loading="lazy">
         <h1 class="page-banner--title heading heading--main" data-aos="zoom-in" data-aos-duration="1000">
            <?= $main_title ?>
         </h1>
         <h2 class="page-banner--subtitle heading--medium" data-aos="fade-up" data-aos-duration="2000">
            <?= $main_description ?>
         </h2>
         <p class="main-contact-adress"><i class="fa fa-building" aria-hidden="true"></i> ul.Warszawska 13/6, 21-500 Biała Podlaska </p>
         <p class="main-contact"><a href="mailto:kancelaria.trebicki@interia.pl"><i class="fa fa-envelope" aria-hidden="true"></i>  kancelaria.trebicki@interia.pl</a></p>
         <p class="main-contact"><a href="tel:733 157 327"><i class="fa fa-phone" aria-hidden="true"></i>  733 157 327</a></p>        
         <a class="main-contact--social" href="https://www.facebook.com/Radca-Prawny-Cezary-Tr%C4%99bicki-100798052403939/?ref=pages_you_manage" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
         <div class="page-banner--button-place">
            <a href="#about-us" class="btn-white">
               <?= $main_button ?>
            </a>
         </div>
      </div>
   </div>
</section>