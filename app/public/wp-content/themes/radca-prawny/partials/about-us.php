<?php
  $about_us_img = get_field('about_us_img');
  $about_us_title = get_field('about_us_title');
  $about_us_description = get_field('about_us_description');
  $about_us_places = get_field('about_us_places');
  $about_us_button = get_field('about_us_button');
  $about_us_second_button = get_field('about_us_second_button');
?>

<div class="container about-us--container" id="about-us">
    <div class="row">
            <div class="offer__headline block-headline">
                <h1 class="about-us-title" data-aos="zoom-in" data-aos-duration="1000" data-aos-once="true">
                    <?= $about_us_title ?>
                </h1>
            </div>
    </div>
    <div class="row about-us">
        <div class="col-lg-6 about-us--img-container" data-aos="fade-right" data-aos-duration="1000" data-aos-once="true">
            <img src="<?= $about_us_img ?>" class="about-us--img" alt="O Kancelarii" loading="lazy">
        </div>
        <div class="col-lg-6 about-us--description-container" data-aos="fade-left" data-aos-duration="1000" data-aos-once="true">
            <p class="about-us--description">
                <?= $about_us_description ?>
            </p>
            <p class="about-us--description">
                <?= $about_us_places ?>
            </p>
            <div class="button-container">
                <a href="#map" class="btn-blue about-us--button">
                    <?= $about_us_button ?>
                </a>
                <a href="#map" class="btn-all-blue about-us--button">
                    <?= $about_us_second_button ?>
                </a>
            </div>

        </div>
    </div>
</div>
