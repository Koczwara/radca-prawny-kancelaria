<footer class="site-footer" id="contact">
      <div class="site-footer__inner container">
        <div class="group">

          <div class="site-footer__col-one">        
            <h6 class="site-footer--title">Kontakt</h6>
            <p><a class="site-footer__link" href="tel:733 157 327">Tel. 733 157 327</a></p>
            <p><a class="site-footer__link" href="mailto:kancelaria.trebicki@interia.pl">kancelaria.trebicki@interia.pl</a></p>  
          </div>

          <div class="site-footer__col-two">
            <h6 class="site-footer--title">Adres</h6>
            <p class="site-footer__link">ul.Warszawska 13/6<br>21-500 Biała Podlaska (ogólnie dostępny parking przed budynkiem). </p>
          </div>
  
          <div class="site-footer__col-three">
            <h6 class="site-footer--title">Godziny Otwarcia</h6>
            <p class="site-footer__link">Pon-Pt: 8:00 - 16:00</p>
            <p class="site-footer__link">Sobota: nieczynne</p>
            <p class="site-footer__link">Niedziela: nieczynne</p>
          </div>

          <div class="site-footer__col-four">
            <h6 class="site-footer--title"></h6>
            <p class="site-footer__link">NIP: 5372661261</p>
            <p class="site-footer__link">Rachunek bankowy PKO BP: 02 1020 1260 0000 0202 0205 7438</p>
          </div>

        </div>
      </div>


      <div class="site-footer__under__inner container">
        <div class="site-footer__under__inner__col-one">        
            © 2021 trebickiprawnik.pl
        </div>
        <div class="site-footer__under__inner__col-two social-icons-list">      
          <a href="https://www.facebook.com/Radca-Prawny-Cezary-Tr%C4%99bicki-100798052403939/?ref=pages_you_manage" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        </div>
        <div class="site-footer__under__inner__col-three">        
          <p>Realizacja: <a href="http://patrycjakoczwara.pl/" target="_blank">patrycjakoczwara.pl</a></p>
        </div>
      </div>

    </footer>
<?php wp_footer(); ?>
</body>
</html>