<?php get_header(); ?>

<?php get_template_part('partials/main'); ?>
<?php get_template_part('partials/about-us'); ?>
<?php get_template_part('partials/services'); ?>
<?php get_template_part('partials/map'); ?>

<?php get_footer(); ?>
