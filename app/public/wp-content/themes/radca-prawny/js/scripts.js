import "../css/style.css"
import $ from "jquery"
import MobileMenu from "./modules/MobileMenu"
import 'bootstrap/dist/css/bootstrap.css';
import AOS from 'aos';
import 'aos/dist/aos.css';



var mobileMenu = new MobileMenu()


if (module.hot) {
  module.hot.accept()
}
// aos

AOS.init();

// menu scroll

window.addEventListener("scroll", function(){
	var header = document.querySelector("header");
	header.classList.toggle("sticky", window.scrollY > 0)
})

 // services
 $('.services .law-container .li a').click(function() {
	var itemID = $(this).attr('href');
	$('.services .law-container').addClass('item_open');
	$(itemID).addClass('item_open');
	return false;
});
$('.close').click(function() {
	$('.port, .services .law-container').removeClass('item_open');
	return false;
});

$(".services .law-container .li a").click(function() {
	$('html, body').animate({
		scrollTop: parseInt($("#top").offset().top)
	}, 400);
});



