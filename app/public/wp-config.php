<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'fSa1MyOvxp3H5pUPEqBsK+qndw4jbO+PmBYxuMYQ5kqdoKEZM32kJh+Jnjh1+p1CUQGg94Mwbxa1CctjcRmVGw==');
define('SECURE_AUTH_KEY',  'f5kgCKqaQvZTXcrPzCII/gwSU0he/y2jt+KV1NipA+lZKAd8oV9zvt638edqE/0qs6bGRgrp2XRJKehDCfreJQ==');
define('LOGGED_IN_KEY',    'EgmMjeumsbCs9fnWnuloYOuYNgyqoHar8QW9oWw7tjCg9ML+3K5idy+L6RmQj7OE1FNXe83JYaMTRWb5ktCoCQ==');
define('NONCE_KEY',        '72MyhlDQObYNgYruvB8ol9yDrVFhHPj6gewJw/qgUFwHPb1M+omUoBWu+Kt/lyA3Hxf12pOYQESrAN5uZ0zIZQ==');
define('AUTH_SALT',        'xqK2G7RyIrMOSKeMIIVgA/u2ztjiapy1yuPK/VRRQ618T1Qd247Tvn6aVRkrfiAguwmeafkV+bAWk2efVud0bQ==');
define('SECURE_AUTH_SALT', '5ctCFCRvgdd3fwYUtRKiAzoBYtlzLKICYdLvBU3zSiOyvvswoZVplPYrcCgHHIAPtucPvrKGUzdTvJmHw6WKFg==');
define('LOGGED_IN_SALT',   'mAqpgEgIGmtA/zOPp5TsSyCDryfrBUpJY6GukfGzlXLsTsn0bxN9RKI4zmA+IbTQeO9J4q2fMCfaMbPajEEN6A==');
define('NONCE_SALT',       'SnuNcmScS/psycMpZoHOYuCUTvSqbHXZskNyrG6WWTre6u9h3+lsCuzka4PsMNlXVDOMie6Zt+G+JGoQAD1iSg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
